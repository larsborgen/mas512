## Mas512 - Object detection via Nvidia Jetson Nano and USB camera
Picture of the Nvidia Jetson in its cover
https://imgur.com/khveoPV

## Description
This git repository provides the neccesary files and information to be able to run object detection via an Nvidia Jetson Nano running Ubuntu and a usb webcamera. 

## Examples of the system running
https://youtu.be/s4jKxL2r8tA

## Installation
Download the repositories for Jetson inference which contains the scripts and files needed to run the object detection system.
If you want to run the system through a ROS node, as to later combine it with other ROS nodes you may have, download the catkin_ws repository too. NB! The files inside the catkin workspace may need to be merged with your folders and files if you already have a ROS workspace.

## Usage
To run using Jetson-Inference: cd to jetson-inference folder, then use command "docker/run.sh" to run the docker.
Starting the object detection system: "detectnet /dev/video0" inside the same directory.

NB! Always source before running ROS: catkin workspace and command "source devel/setup.bash"

To run image Node (camera):
roslaunch ros_deep_learning video_viewer.ros1.launch input:=/dev/video0 output:=display://0

To run detectnet Node (object detection):
roslaunch ros_deep_learning detectnet.ros1.launch input:=/dev/video0 output:=display://0

To run segnet Node (Segmentation):
roslaunch ros_deep_learning segnet.ros1.launch input:=/dev/video0 output:=display://0

If you want to run with another input, instead of a webcam. A list of inputs can be found here:
https://github.com/dusty-nv/jetson-inference/blob/master/docs/aux-streaming.md

## Support
If there is a need for help or if you are stuck, i suggest looking at Nvidias playlist on youtube for Jetson AI fundamentals
I will also include the link here so it is easy to find https://www.youtube.com/playlist?list=PL5B692fm6--uQRRDTPsJDp4o0xbzkoyf8

## Authors and acknowledgment
This project is built upon the knowledge gathered from Dustin Franklin`s github pages for "Jetson-inference", and "Deep learning nodes", on git his username is Dusty-nv.


## License
From Jetson-inference:: https://github.com/dusty-nv/jetson-inference ::
For open source projects, say how it is licensed.

Copyright (c) 2017, NVIDIA CORPORATION. All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE. */



